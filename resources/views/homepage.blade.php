<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="token" content="{{ csrf_token() }}">
        <title>Yuan's Kitchen Garden</title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/carousel.css')}}" rel="stylesheet">
        @yield('header')
    </head>
    <body>
        <nav class="navbar navbar-inverse" style="border-radius: 0;">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">Yuan's Kitchen Garden</a>
                <ul class="nav navbar-nav">
                    <li @if(Route::current()->getName() == 'home') class="active" @endif><a href="{{ route('home') }}">Home</a></li>
                    @if (Route::has('login'))
                        @auth
                            @if(auth()->user()->role=='admin')
                            <li @if(Route::current()->getName() == 'admin') class="active" @endif><a href="{{ route('admin') }}">Admin Panel</a></li>
                            @endif
                            <li @if(Route::current()->getName() == 'orders') class="active" @endif><a href="{{ route('orders.index') }}">Orders</a></li>
                            <li id="cart-nav" @if(Route::current()->getName() == 'cart') class="active" @endif><a href="{{ route('cart.index') }}"><i class="glyphicon glyphicon-shopping-cart"></i> Cart (<span>{{auth()->user()->cart()}}</span>)</a></li>
                            
                        @else
                            <li @if(Route::current()->getName() == 'login') class="active" @endif><a href="{{ route('login') }}">Login</a></li>
                            @if (Route::has('register'))
                                <li @if(Route::current()->getName() == 'register') class="active" @endif><a href="{{ route('register') }}">Register</a></li>
                            @endif
                        @endauth
                    @endif
                </ul>
                @auth
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </li>
                </ul>
                @endauth
            </div>
        </nav>
        
        @yield('content')

        <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/toastr.min.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        @yield('footer')
        @if(session('success'))
            <script>toastr.success('{{ session('success') }}', 'Success!')</script>
        @endif
        @if(session('error'))
            <script>toastr.error('{{ session('error') }}', 'Error!')</script>
        @endif
    </body>
</html>