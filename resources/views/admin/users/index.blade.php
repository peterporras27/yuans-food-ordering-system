@extends('admin.index')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Users</h3>
                </div>
                <div class="panel-body">
                    
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#prods" aria-controls="prods" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Lists</a>
                            </li>
                            <li role="presentation">
                                <a href="#addprods" aria-controls="addprods" role="tab" data-toggle="tab"><i class="fa fa-plus"></i> Register New User</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif

                            <div role="tabpanel" class="tab-pane active" id="prods">
                                <br>
                                @if($data->count())

                                <div class="table-responsive">
                                    
                                    <table class="table table-bordered table-striped ">
                                        <tr>
                                            <th>Username</th>
                                            <th>Name</th>
                                            <th>Account Status</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th width="280px">Action</th>
                                        </tr>
                                        @foreach ($data as $key => $user)
                                        <tr>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                            <td>
                                                @if($user->verified_member)
                                                <span class="badge badge-pill alert-success">Verified</span>
                                                @else
                                                <span class="badge badge-pill badge-default">Not Verified</span>
                                                @endif
                                            </td>
                                            <td>{{ $user->email }}</td>
                                            <td><span class="badge badge-pill alert-info">{{ $user->role }}</span></td>
                                            <td>
                                                <a class="btn btn-primary btn-xs" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                                <form action="{{ route('users.destroy', $user->id) }}" method="DELETE" style="display:inline;">
                                                    @csrf
                                                    <input type="submit" class="btn btn-danger btn-xs" value="Delete">
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>

                                </div>
                               
                                {!! $data->render() !!}

                                @else
                                <div class="alert alert-warning">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <strong>Opps!</strong> there are no users to display at the moment.
                                </div>
                                @endif
                            </div>
                            <div role="tabpanel" class="tab-pane" id="addprods">
                                <br>
                                <form action="{{ route('users.store') }}" method="POST" role="form">
                                    @csrf
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>Username:</strong>
                                                <input type="text" name="username" placeholder="Username" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Email:</strong>
                                                <input type="email" name="email" placeholder="Email" class="form-control">
                                            </div>
                                             <div class="form-group">
                                                <strong>Password:</strong>
                                                <input type="password" name="password" placeholder="Password" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Confirm Password:</strong>
                                                <input type="password" name="confirm-password" placeholder="Confirm Password" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Role:</strong>
                                                <select name="role" class="form-control">
                                                    <option value="customer">Customer</option>
                                                    <option value="admin">Administrator</option>
                                                </select>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="form-group">
                                                        <strong>Verification Code:</strong><br>
                                                        <input type="text" name="code" class="form-control" value="{{ $code }}"> 
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-8">
                                                    <div class="form-group">
                                                        <strong>Verified Member:</strong><br>
                                                        <input type="checkbox" name="verified_member" class="form-control js-switch" value="1" checked> 
                                                        <span class="avail badge badge-pill badge-info">Verified</span>
                                                        <small>&nbsp;Verified members are allowed to checkout and purchase orders.</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <strong>First Name:</strong>
                                                <input type="text" name="first_name" placeholder="First Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Last Name:</strong>
                                                <input type="text" name="last_name" placeholder="Last Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Middle Name:</strong>
                                                <input type="text" name="middle_name" placeholder="Middle Name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Address:</strong>
                                                <input type="text" name="address" placeholder="Address" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Phone:</strong>
                                                <input type="text" name="phone" placeholder="095...." class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <strong>Birth Day:</strong>
                                                <input type="text" name="birthday" placeholder="dd/mm/yyyy" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <button type="submit" class="btn btn-primary">Submit <i class="fa fa-save"></i></button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div><!-- panel-body -->
            </div><!-- panel -->

           
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    var $ = jQuery;

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
      var switchery = new Switchery(html);
    });

    var changeCheckbox = document.querySelector('.js-switch');
    changeCheckbox.onchange = function() {
        if (changeCheckbox.checked) {
            $('.avail').text('Verified');
            $('#product-available').val(1);
        } else {
            $('.avail').text('Non Verified');
            $('#product-available').val(0);
        }
    };
</script>
@endsection