@extends('admin.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Transaction</h3>
				</div>
				<div class="panel-body">
					
					<div class="row">
						<div class="col-md-6">
						
							<div class="table-responsive">
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th>Name</th>
											<th>Details</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Transaction ID:</td>
											<td>{{ strtoupper($trans->transaction_code) }}</td>
										</tr>
										<tr>
											<td>Name:</td>
											<td>{{ $trans->first_name.' '.$trans->last_name }}</td>
										</tr>
										<tr>
											<td>Address:</td>
											<td>{{ $trans->address }}</td>
										</tr>
										<tr>
											<td>Phone:</td>
											<td>{{ $trans->phone }}</td>
										</tr>
										<tr>
											<td>Order Status:</td>
											<td>
												<form action="{{ route('transaction.update', $trans->id) }}" method="POST">
		                        					{{ method_field('PATCH') }}
		                        					@csrf
		                        					<div class="input-group">
		                        						<select class="form-control" name="status">
		                        							<option value="hold"{{ $trans->status=='hold' ? ' selected':'' }}>On Hold</option>
		                        							<option value="pending"{{ $trans->status=='pending' ? ' selected':'' }}>Pending</option>
		                        							<option value="processing"{{ $trans->status=='processing' ? ' selected':'' }}>Processing</option>
		                        							<option value="delivered"{{ $trans->status=='delivered' ? ' selected':'' }}>Delivered</option>
		                        							<option value="failed"{{ $trans->status=='failed' ? ' selected':'' }}>Canceled / Failed</option>
		                        						</select>
		                        						<span class="input-group-btn">
		                        							<button class="btn btn-success" type="submit">Update Status</button>
		                        						</span>
		                        					</div><!-- /input-group -->
												</form>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-md-6">
							<div class="table-responsive">
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th>Name</th>
											<th>Quantity</th>
											<th>Price</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<?php $total = 0; $count = 0; ?>
										@foreach($carts as $cart)
										<tr>
											<td>{{ $cart->product_name }}</td>
											<td>{{ $cart->quantity }}</td>
											<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
											<td>₱{{ number_format( ($cart->price*$cart->quantity) ,2,'.',',') }}</td>
										</tr>
										<?php $total = $total+$cart->price; $count = $count+$cart->quantity; ?>
										@endforeach	
										<tr class="success">
											<td><b>TOTAL:</b></td>
											<td class="quantity">{{$count}}</td>
											<td></td>
											<td><b class="grand">₱{{ number_format($total,2,'.',',') }}</b></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>	
</div>
@endsection