@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 500px;}
.cat .thumbnail{
	overflow: hidden;
}
.cat .img{
	height: 300px;
	background-size:cover !important;
}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container" align="center">
		<h1>Yuan's Kitchen Garden</h1>
		<p>
			Capture Beautiful Moments At Yuan's!
		</p>
	</div>
</div>

<div class="container marketing" align="center">

	@foreach($categories as $category)
	<a href="#" onclick="showHide(event,{{ $category->id }})" class="btn btn-default" style="margin-bottom: 15px;">{{ $category->name }}</a>
	@endforeach
	<hr>
	<div class="products">
		
		@foreach($products as $product)

			<div class="col-lg-4 cat cat-{{$product->category_id}}">
				
				<div class="thumbnail">
				@if($product->images->first())
					<div class="img" style="background:url('{{ route('image',$product->images->first()->id) }}') no-repeat center center;"></div>
				@else
					<div class="img" style="background:url('{{asset('assets/images/placeholder.png')}}') no-repeat center center;"></div>
				@endif
				</div>
				
				<h2>{{$product->name}}</h2>
				<div class="description">{{$product->description}}</div>
				<p><button onclick="addToCart({{$product->id}})" class="btn btn-default">₱{{$product->price}} Add to cart &raquo;</button></p>
			</div>

		@endforeach	
	</div>
</div>

@endsection
@section('footer')
<script>
var $=jQuery;
function showHide(e,id){
	e.preventDefault();
	var $=jQuery;
	
	$('.cat').hide();
	$('.cat-'+id).fadeIn();
}
function addToCart(id){

	$.ajax({
		url: '{{route('add')}}',
		type: 'POST',
		dataType: 'json',
		data: {id:id,_token:'{{csrf_token()}}',quantity:1},
	}).always(function(res) {

		console.log( res );

		if (res.error) {

			toastr.warning(res.message, 'Error!');

		} else {

			toastr.success(res.message, 'Success!');
			$('#cart-nav').find('span').text(res.count);
		}
	});
}
</script>
@endsection