@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container">
		<h1>Order Details</h1>
	</div>
</div>

<div class="container marketing">
	<div class="row">
		
		<div class="col-md-12">

			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Transaction #{{ strtoupper($trans->transaction_code) }}</h3>
				</div>
				<div class="panel-body">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<p><strong>Order Successful!</strong></p>
						<p>Your order is on it's way, kindly prepare the total amount of denomination you order upon the arival of our delivery crew.</p>
						<p>Thank you for trusting us! <b>Enjoy your food!</b> <a href="/" style="color:#5a5a5a;">Order more menu</a></p>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<?php $total = 0; $count = 0; ?>
								@foreach($carts as $cart)
								<tr>
									<td>{{ $cart->product_name }}</td>
									<td>{{ $cart->quantity }}</td>
									<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
									<td>₱{{ number_format( ($cart->price*$cart->quantity) ,2,'.',',') }}</td>
								</tr>
								<?php $total = $total+$cart->price; $count = $count+$cart->quantity; ?>
								@endforeach	
								<tr class="success">
									<td><b>TOTAL:</b></td>
									<td class="quantity">{{$count}}</td>
									<td></td>
									<td><b class="grand">₱{{ number_format($total,2,'.',',') }}</b></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Details</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Transaction ID:</td>
									<td>{{ strtoupper($trans->transaction_code) }}</td>
								</tr>
								<tr>
									<td>Name:</td>
									<td>{{ $trans->first_name.' '.$trans->last_name }}</td>
								</tr>
								<tr>
									<td>Address:</td>
									<td>{{ $trans->address }}</td>
								</tr>
								<tr>
									<td>Phone:</td>
									<td>{{ $trans->phone }}</td>
								</tr>
								<tr>
									<td>Order Status:</td>
									<td><span class="badge badge-pill badge-info">{{ $trans->status }}</span></td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
			</div>
			
		</div>
		
	</div>
</div>

@endsection
@section('footer')
<script>
var $=jQuery;
</script>
@endsection