@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container">
		<h1>Order List</h1>
	</div>
</div>

<div class="container marketing">
	<div class="row">
		
		<div class="col-md-12">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Transactions</h3>
				</div>
				<div class="panel-body">
					
					@if($orders->count())

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Option</th>
										<th>Transaction Code</th>
										<th>Status</th>
										<th>Address</th>
										<th>Phone</th>
										<th>Customer Name</th>
										<th>Amount</th>
									</tr>
								</thead>
								<tbody>
									@foreach($orders as $order)
									<tr class="{{ $status[$order->status] }}">
										<td><a class="btn btn-default btn-xs" href="{{ route('order',$order->id) }}">View details</a></td>
										<td>{{ strtoupper($order->transaction_code) }}</td>
										<td><span class="badge">{{ ucfirst($order->status) }}</span></td>
										<td>{{ $order->address }}</td>
										<td>{{ $order->phone }}</td>
										<td>{{ $order->first_name.' '.$order->last_name }}</td>
										<td>₱{{ number_format($order->total_price,2,'.',',') }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						{!! $orders->render() !!}

					@else
						<div class="alert alert-info">
							<strong>Oops</strong> There are no records to show at the moment.
						</div>
					@endif

				</div>
			</div>
			
		</div>
		
	</div>
</div>

@endsection
@section('footer')
<script>
var $=jQuery;
</script>
@endsection