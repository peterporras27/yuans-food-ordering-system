@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
.tab-content{
	padding: 15px;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;

    border-bottom-right-radius: 5px;
    border-bottom-left-radius: 5px;
    border-top-right-radius: 5px;
}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container" align="center">
		<h1>Cart Order Information</h1>
	</div>
</div>

<div class="container marketing">

	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active">
				<a href="#home" aria-controls="home" role="tab" data-toggle="tab" style="color: #000;">Cart Details</a>
			</li>
			<li role="presentation">
				<a id="checkout" href="#tab" aria-controls="tab" role="tab" data-toggle="tab" style="color: #000;">Checkout</a>
			</li>
		</ul>
	
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">
				<div class="table-responsive">
					<table class="table table-hover table-bordered">
						<thead>
							<tr class="info">
								<th>Option</th>
								<th>Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $total = 0; $count = 0; ?>
							@foreach($carts as $cart)
								<tr>
									<td>
										<form action="{{ route('cart.destroy', $cart->id) }}" method="POST" style="display:inline;">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-xs">Remove <i class="glyphicon glyphicon-remove"></i></button>
                                        </form>
									</td>
									<td>{{ $cart->product_name }}</td>
									<td>
										<input onchange="cartUpdate(this,{{$cart->product_id}})" data-price="{{ $cart->price }}" type="number" min="1" value="{{ $cart->quantity }}">
									</td>
									<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
									<td id="total-{{$cart->product_id}}">₱{{ number_format(($cart->price*$cart->quantity),2,'.',',') }}</td>
								</tr>
								<?php $total = $total+$cart->price; $count = $count+$cart->quantity; ?>
							@endforeach	
							<tr class="success">
								<td><b>TOTAL:</b></td>
								<td></td>
								<td class="quantity">{{$count}}</td>
								<td></td>
								<td><b class="grand">₱{{ number_format($total,2,'.',',') }}</b></td>
							</tr>
						</tbody>
					</table>
					<a href="#tab" class="btn btn-primary" onclick="jQuery('#checkout').click();">Next Step <i class="glyphicon glyphicon-chevron-right"></i></a>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="tab" align="left">
				<form action="{{ route('cart.store') }}" method="POST" role="form">
					@csrf
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>First Name:</label>
								<input type="text" name="first_name" class="form-control" value="{{ old('first_name') ? old('first_name') : auth()->user()->first_name }}" required>
							</div>
							<div class="form-group">
								<label>Last Name:</label>
								<input type="text" name="last_name" class="form-control" value="{{ old('last_name') ? old('last_name') : auth()->user()->last_name }}" required>
							</div>
							<div class="form-group">
								<label>Phone:</label>
								<input type="text" name="phone" class="form-control" value="{{ old('phone') ? old('phone') : auth()->user()->phone }}" required>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Address:</label>
								<input type="text" name="address" class="form-control" value="{{ old('address') ? old('address') : auth()->user()->address }}" required>
							</div>
							<div class="form-group">
								<label>Order Notes:</label>
								<textarea rows="3" name="notes" class="form-control" placeholder="Special requests or other."></textarea>
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-success">Request For Delivery <i class="glyphicon glyphicon-ok"></i></button>
				</form>
			</div>
		</div>
	</div>

	

</div>

@endsection
@section('footer')
<script>
var $=jQuery;

function cartUpdate(input,id){

	var quantity = parseInt($(input).val());
	var price = $(input).data('price');

	var formatter = new Intl.NumberFormat('en-PH', {
	  style: 'currency',
	  currency: 'PHP',

	  // These options are needed to round to whole numbers if that's what you want.
	  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	});

	$('#total-'+id).html( formatter.format((parseFloat(price)*quantity)) );

	$.ajax({
		url: '{{route('add')}}',
		type: 'POST',
		dataType: 'json',
		data: {id:id,_token:'{{csrf_token()}}',quantity:quantity},
	}).always(function(res) {

		console.log( res );
		var total = 0;
		var grandTotal = 0;
		$('[type="number"]').each(function(index, el) {
			var quantity = parseInt($(this).val());
			var price = $(this).data('price');
			total = parseInt(total)+quantity;
			grandTotal = grandTotal+(parseFloat(price)*quantity);
		});

		$('.quantity').text(total);
		$('.grand').text(formatter.format(grandTotal));

		if (res.error) {

			toastr.warning('Oops! something went wrong.', 'Error!');

		} else {

			toastr.success('Cart Updated!', 'Success!');
		}


	});
}
</script>
@endsection