<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'price', 'available','category_id'
    ];

    public function images()
    {
    	return $this->hasMany('App\Images','product_id');
    }

    public function category()
    {
    	return $this->hasOne('App\Category','id','category_id');
    }
}
