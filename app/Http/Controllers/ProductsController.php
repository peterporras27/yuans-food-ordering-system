<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Images;
use Validator;
use Image;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);

        $this->validation = [
            'name'        => 'required|string|min:3|max:255',
            'description' => 'required|string|min:5',
            'price'       => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'category_id' => 'required|integer|max:255'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['categories'] = Category::all();
        $params['products'] = Product::paginate(15);
        
        return view('admin.products.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            $msg = $validator->messages();
            return redirect('products')->with('error', $validator->messages());
        }

        $product = new Product;
        $product->fill( $request->all() );
        $product->save();

        if( $request->hasFile('photos') )
        {
            $allowedfileExtension = ['jpg','png','jpeg','gif'];

            $files = $request->file('photos');

            foreach($files as $file)
            {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array( $extension, $allowedfileExtension );

                if($check)
                {
                    foreach ($request->photos as $photo) 
                    {
                        $filename = $photo->store('uploads');
                        
                        if ($filename) 
                        {
                            $img = new Images;
                            $img->filename = $filename;
                            $img->product_id = $product->id;
                            $img->save();   
                        }
                    }
                }
            }
        }

        return redirect('products')->with('success','Product succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $params['categories'] = Category::all();
        $params['product'] = Product::find( $id );
        $params['images'] = Images::where( 'product_id', '=', $id )->get();

        return view('admin.products.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if (!$product) {
            return redirect('products')->with('error', 'Product no longer exist.');
        }

        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            $msg = $validator->messages();
            return redirect('products/'.$id.'/edit')->with('error', $validator->messages());
        }

        $product->fill( $request->all() );
        $product->available = ($request->input('available'))? 1:0;
        $product->save();

        if($request->hasFile('photos'))
        {
            $allowedfileExtension = ['jpg','png','jpeg','gif'];

            $files = $request->file('photos');

            foreach($files as $file)
            {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension,$allowedfileExtension);

                if($check)
                {
                    foreach ($request->photos as $photo) 
                    {
                        $filename = $photo->store('uploads');

                        if ($filename) 
                        {
                            $img = new Images;
                            $img->filename = $filename;
                            $img->product_id = $product->id;
                            $img->save();
                        }
                    }
                }
            }
        }

        return redirect('products/'.$id.'/edit')->with('success','Product succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
