<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Cart;
use Validator;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required|string',
            'phone' => 'required|string'
        ];
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Cart::where([
            ['user_id','=',auth()->user()->id],
            ['status','=','hold']
        ])->get();

        if ($carts->count()==0) {
            return redirect('/')->with('error','Cart is empty.');
        }

        return view('public.cart',compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('cart')->with('error', $validator->messages());
        }

        // pull all cart
        $carts = Cart::where([
            ['user_id','=',auth()->user()->id],
            ['status','=','hold']
        ])->get();

        if ($carts->count() == 0) {
            return redirect('/')->with('error', 'Cart is empty.');
        }

        if (!auth()->user()->verified_member) {
            return redirect('/verify')->with('error', 'Kindly verify your account.');       
        }

        $total = 0;
        $transaction_code = $this->randStr();

        foreach ($carts as $cart) {
            $total = ($cart->price*$cart->quantity)+$total;
            $cart->transaction_code = $transaction_code;
            $cart->status = 'pending';
            $cart->save();
        }

        $trans = new Transaction;
        $trans->transaction_code = $transaction_code;
        $trans->first_name = $request->input('first_name');
        $trans->last_name = $request->input('last_name');
        $trans->address = $request->input('address');
        $trans->phone = $request->input('phone');
        $trans->total_price = $total;
        $trans->user_id = auth()->user()->id;
        $trans->status = 'pending';
        $trans->save();

        return redirect('/order/'.$trans->id)->with('success', 'Order successful!');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::find($id)->delete();
        return redirect()->route('cart.index')
                        ->with('success','Cart item deleted successfully');
    }

    public function randStr($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
