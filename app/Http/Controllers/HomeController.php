<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Images;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders = Transaction::paginate(20);
        $status = array(
            'hold' => '',
            'pending' => 'warning',
            'processing' => 'info',
            'delivered' => 'success',
            'failed' => 'danger'
        );

        return view('admin.transactions',compact('orders','status'));
    }

    public function deleteImage($id)
    {

        $img = Images::find($id);

        $return = array(
            'error' => true,
            'message' => 'please try again',
        );

        if ($img) {

            if ( Storage::exists($img->filename) ) 
            {
                Storage::delete($img->filename);
            }

            $return['error'] = false;
            $return['message'] = 'Image removed successfully.';
            $img->delete();
        }

        return $return;
    }
}
