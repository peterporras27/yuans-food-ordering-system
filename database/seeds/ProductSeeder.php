<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	['name' => 'Crispy Pata','description' => ' ','price' => 499.00,'category_id' => 1],
        	['name' => 'Sizzling Bistek Tagalog','description' => ' ','price' => 275.00,'category_id' => 1],
        	['name' => 'Sizzling Bulalo Steak','description' => ' ','price' => 299.00,'category_id' => 1],
        	['name' => 'Bulalo Soup','description' => ' ','price' => 299.00,'category_id' => 1],
        	['name' => 'Pork Sisig','description' => ' ','price' => 299.00,'category_id' => 1],
        	['name' => 'Sizzling Burger Steak','description' => 'Additional Mash Potato (Php 5.00)','price' => 299.00,'category_id' => 1],

        	['name' => 'Fish Fillet','description' => 'With Basil & Tartar Sauce','price' => 275.00,'category_id' => 2],
        	['name' => 'Sizzling Bangus Sisig','description' => 'in Olive Oil','price' => 275.00,'category_id' => 2],
        	['name' => 'Grilled Bangus','description' => ' ','price' => 275.00,'category_id' => 2],
        	['name' => 'Sinigang na Tangigue','description' => ' ','price' => 330.00,'category_id' => 2],
        	['name' => 'Grilled Tangigue','description' => ' ','price' => 330.00,'category_id' => 2],

        	['name' => 'Hawaiian','description' => ' ','price' => 230.00,'category_id' => 3],
        	['name' => 'Pepperoni','description' => ' ','price' => 230.00,'category_id' => 3],
        	['name' => 'Four Cheese','description' => ' ','price' => 230.00,'category_id' => 3],
        	['name' => 'Vegetarian','description' => ' ','price' => 230.00,'category_id' => 3],
        	['name' => 'Pizza With Iced Tea Picher','description' => ' ','price' => 300.00,'category_id' => 3],

        	['name' => 'Carbonara Fettuccine Spaghetti (Solo)','description' => ' ','price' => 140.00,'category_id' => 4],
        	['name' => 'Carbonara Fettuccine Spaghetti (Group)','description' => ' ','price' => 230.00,'category_id' => 4],

        	['name' => 'Chicken in the Basket','description' => ' ','price' => 249.00,'category_id' => 5],
        	['name' => 'Chicken Cordon Blue','description' => ' ','price' => 249.00,'category_id' => 5],

        	['name' => 'Fries Overload','description' => ' ','price' => 115.00,'category_id' => 6],
        	['name' => 'Burger ala Yuan','description' => ' ','price' => 135.00,'category_id' => 6],
        	['name' => 'Mojos with iced tea','description' => ' ','price' => 149.00,'category_id' => 6],
        	['name' => 'Nachos Platter','description' => ' ','price' => 200.00,'category_id' => 6],
        	['name' => 'Spicy Buffalo Wings','description' => ' ','price' => 200.00,'category_id' => 6],
        	['name' => 'Calamares','description' => ' ','price' => 210.00,'category_id' => 6],
        	['name' => 'Pancit Molo','description' => ' ','price' => 230.00,'category_id' => 6],
        	['name' => 'Sotanghon Guisado','description' => ' ','price' => 230.00,'category_id' => 6],
        	['name' => 'Bihon Guisado','description' => ' ','price' => 230.00,'category_id' => 6],

        	['name' => 'Long Island Iced Tea','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'Frozen Margarita','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'Blue Frozen Margarita','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'White Russian','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'Sangiria','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'Piña Colada','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'Zombie','description' => ' ','price' => 120.00,'category_id' => 7],
        	['name' => 'Mai-tai','description' => ' ','price' => 120.00,'category_id' => 7],

        	['name' => 'Lemonade','description' => ' ','price' => 195.00,'category_id' => 8],
        	['name' => 'Cucumber Lemon','description' => ' ','price' => 195.00,'category_id' => 8],
        	['name' => 'Iced Tea (Pitcher)','description' => ' ','price' => 195.00,'category_id' => 8],
        	['name' => 'Iced Tea (Tower)','description' => ' ','price' => 299.00,'category_id' => 8],
        	['name' => 'Soda','description' => ' ','price' => 50.00,'category_id' => 8],
        	['name' => 'Mineral Water','description' => ' ','price' => 50.00,'category_id' => 8],
        	['name' => 'Yuan\'s Smoothies','description' => ' ','price' => 115.00,'category_id' => 8],
        	['name' => 'Shakes','description' => 'Mango, Avocado, Laychee, Buko, Pinapple, Apple, Orange','price' => 115.00,'category_id' => 8],

        	['name' => 'Chopsuey Platter','description' => ' ','price' => 249.00,'category_id' => 9],

        	['name' => 'Plain Rice','description' => ' ','price' => 30.00,'category_id' => 10],
        	['name' => 'Garlic Rice','description' => ' ','price' => 40.00,'category_id' => 10],
        	['name' => 'Rice Platter (Plain)','description' => ' ','price' => 170.00,'category_id' => 10],
        	['name' => 'Rice Platter (Garlic)','description' => ' ','price' => 200.00,'category_id' => 10],

        	['name' => 'Yuan\'s Foodie Choice Assorted Sausage Platter (US Made)','description' => ' ','price' => 439.00,'category_id' => 11],
        	['name' => 'Yuan\'s Special Grilled Backribs','description' => 'with seasoned mashed potatoes and steamed veggies','price' => 410.00,'category_id' => 11],

        	['name' => 'Ice Cream (Per Scoop)', 'description' => '','price' => 50.00,'category_id' => 12],
        	['name' => 'Halo Halo', 'description'=> '','price' => 135.00,'category_id' => 12]
        	
        ]);

    }
}
